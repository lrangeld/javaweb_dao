package won.luisrangel.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.javabeans.Actor;

/**
 *
 * @author ljrangeld
 */
public interface ActorDAO {

    public boolean insert(Actor a) throws SQLException;

    public boolean update(Actor a) throws SQLException;

    public boolean delete(int id) throws SQLException;

    public Actor select(int id) throws SQLException;

    public ArrayList<Actor> selectAll() throws SQLException;
    
    public ArrayList<Actor> selectFilter(String criteria) throws SQLException;
}
