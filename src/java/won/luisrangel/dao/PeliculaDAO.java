package won.luisrangel.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.javabeans.Pelicula;

/**
 *
 * @author ljrangeld
 */
public interface PeliculaDAO {

    public boolean insert(Pelicula p) throws SQLException;

    public boolean update(Pelicula p) throws SQLException;

    public boolean delete(int id) throws SQLException;

    public Pelicula select(int id) throws SQLException;

    public ArrayList<Pelicula> selectAll() throws SQLException;
    
    public ArrayList<Pelicula> selectFilter(String criteria) throws SQLException;
}
