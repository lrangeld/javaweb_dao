package won.luisrangel.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.javabeans.Genero;

/**
 *
 * @author ljrangeld
 */
public interface GeneroDAO {

    public boolean insert(Genero g) throws SQLException;

    public boolean update(Genero g) throws SQLException;

    public boolean delete(int id) throws SQLException;

    public Genero select(int id) throws SQLException;

    public ArrayList<Genero> selectAll() throws SQLException;
    
    public ArrayList<Genero> selectFilter(String criteria) throws SQLException;
}
