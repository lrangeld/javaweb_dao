package own.luisrangel.dao.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.db.Conexion;
import own.luisrangel.javabeans.Actor;
import won.luisrangel.dao.ActorDAO;

/**
 *
 * @author ljrangeld
 */
public class ActorImplement implements ActorDAO {

    @Override
    public boolean insert(Actor a) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String insertCommand = "INSERT INTO " + TABLE_NAME + "(name) VALUES(?);";
        try (PreparedStatement st = conexion.prepareStatement(insertCommand)) {
            st.setString(1, a.getName());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public boolean update(Actor a) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String updateCommand = "UPDATE " + TABLE_NAME + " SET name = ? WHERE idActor = ?";
        try (PreparedStatement st = conexion.prepareStatement(updateCommand)) {
            st.setString(1, a.getName());
            st.setInt(2, a.getIdActor());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String deleteCommand = "DELETE FROM " + TABLE_NAME + " WHERE idActor = ?";
        try (PreparedStatement st = conexion.prepareStatement(deleteCommand)) {
            st.setInt(1, id);

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public Actor select(int id) throws SQLException {
        Actor a = null;
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME + " WHERE idActor = ?";
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    a = new Actor(rs.getInt(1), rs.getString(2));
                }
            }
        }
        return a;
    }

    @Override
    public ArrayList<Actor> selectAll() throws SQLException {
        ArrayList<Actor> a = new ArrayList();
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME;
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    a.add(new Actor(rs.getInt(1), rs.getString(2)));
                }
            }
        }
        return a;
    }
    
    @Override
    public ArrayList<Actor> selectFilter(String criteria) throws SQLException {
        ArrayList<Actor> a = new ArrayList();
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM Actor WHERE name LIKE ?";
        try {
            try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
                st.setString(1, "%" + criteria + "%");
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        a.add(new Actor(rs.getInt(1), rs.getString(2)));
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("PeliculaImplement: " + e.getMessage());
        }
        conexion.close();
        return a;
    }

    //Atributos de calse
    private static final String TABLE_NAME = "Actor";
    private Connection conexion = null;
}
