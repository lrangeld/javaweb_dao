package own.luisrangel.dao.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.db.Conexion;
import own.luisrangel.javabeans.Pelicula;
import won.luisrangel.dao.PeliculaDAO;

/**
 *
 * @author ljrangeld
 */
public class PeliculaImplement implements PeliculaDAO {

    @Override
    public boolean insert(Pelicula p) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String insertCommand = "INSERT INTO " + TABLE_NAME + "(name, year, synopsis) VALUES(?, ?, ?);";
        try (PreparedStatement st = conexion.prepareStatement(insertCommand)) {
            st.setString(1, p.getName());
            st.setInt(2, p.getYear());
            st.setString(3, p.getSynopsis());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        conexion.close();
        return success;
    }

    @Override
    public boolean update(Pelicula p) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String updateCommand = "UPDATE " + TABLE_NAME + " SET name = ?, year = ?, synopsis = ? WHERE id = ?";
        try (PreparedStatement st = conexion.prepareStatement(updateCommand)) {
            st.setString(1, p.getName());
            st.setInt(2, p.getYear());
            st.setString(3, p.getSynopsis());
            st.setInt(4, p.getId());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        conexion.close();
        return success;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String deleteCommand = "DELETE FROM " + TABLE_NAME + " WHERE id = ?";
        try (PreparedStatement st = conexion.prepareStatement(deleteCommand)) {
            st.setInt(1, id);

            success = (st.executeUpdate() > 0) ? true : false;
        }
        conexion.close();
        return success;
    }

    @Override
    public Pelicula select(int id) throws SQLException {
        Pelicula p = null;
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME + " WHERE id = ?";
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    p = new Pelicula(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4));
                }
            }
        }
        conexion.close();
        return p;
    }

    @Override
    public ArrayList<Pelicula> selectAll() throws SQLException {
        ArrayList<Pelicula> p = new ArrayList();
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME;
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    p.add(new Pelicula(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
                }
            }
        }
        conexion.close();
        return p;
    }

    @Override
    public ArrayList<Pelicula> selectFilter(String criteria) throws SQLException {
        ArrayList<Pelicula> p = new ArrayList();
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT p.id, p.name, p.year, p.synopsis /*,g.name*/ FROM " + TABLE_NAME + " p\n"
                + "INNER JOIN " + TABLE_INNER01 + " gp ON p.id =  gp.idPeli\n"
                + "INNER JOIN " + TABLE_INNER02 + " g ON g.idGenero = gp.idGenero\n"
                + "WHERE p.year LIKE ? OR g.name LIKE ?\n"
                + "GROUP BY p.name, p.year, p.synopsis;";
        try {
            try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
                st.setString(1, "%" + criteria + "%");
                st.setString(2, "%" + criteria + "%");
                try (ResultSet rs = st.executeQuery()) {
                    while (rs.next()) {
                        p.add(new Pelicula(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4)));
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("PeliculaImplement: " + e.getMessage());
        }
        conexion.close();
        return p;
    }

    //Atributos de calse
    private static final String TABLE_NAME = "Pelicula";
    private static final String TABLE_INNER01 = "GeneroPelicula";
    private static final String TABLE_INNER02 = "Genero";
    private Connection conexion = null;

}
