package own.luisrangel.dao.implement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import own.luisrangel.db.Conexion;
import own.luisrangel.javabeans.Genero;
import won.luisrangel.dao.GeneroDAO;

/**
 *
 * @author ljrangeld
 */
public class GeneroImplement implements GeneroDAO {

    @Override
    public boolean insert(Genero g) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String insertCommand = "INSERT INTO " + TABLE_NAME + "(name) VALUES(?);";
        try (PreparedStatement st = conexion.prepareStatement(insertCommand)) {
            st.setString(1, g.getName());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public boolean update(Genero g) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String updateCommand = "UPDATE " + TABLE_NAME + " SET name = ? WHERE idGenero = ?";
        try (PreparedStatement st = conexion.prepareStatement(updateCommand)) {
            st.setString(1, g.getName());
            st.setInt(2, g.getIdGenero());

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public boolean delete(int id) throws SQLException {
        boolean success = false;
        conexion = Conexion.getDBConection();
        String deleteCommand = "DELETE FROM " + TABLE_NAME + " WHERE idGenero = ?";
        try (PreparedStatement st = conexion.prepareStatement(deleteCommand)) {
            st.setInt(1, id);

            success = (st.executeUpdate() > 0) ? true : false;
        }
        return success;
    }

    @Override
    public Genero select(int id) throws SQLException {
        Genero g = null;
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME + " WHERE idGenero = ?";
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            st.setInt(1, id);
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    g = new Genero(rs.getInt(1), rs.getString(2));
                }
            }
        }
        return g;
    }

    @Override
    public ArrayList<Genero> selectAll() throws SQLException {
        ArrayList<Genero> g = new ArrayList();
        conexion = Conexion.getDBConection();
        String selectCommand = "SELECT * FROM " + TABLE_NAME;
        try (PreparedStatement st = conexion.prepareStatement(selectCommand)) {
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    g.add(new Genero(rs.getInt(1), rs.getString(2)));
                }
            }
        }
        return g;
    }

    @Override
    public ArrayList<Genero> selectFilter(String criteria) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }   
    
    //Atributos de calse
    private static final String TABLE_NAME = "Genero";
    private Connection conexion = null;
}
