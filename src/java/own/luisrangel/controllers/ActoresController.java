package own.luisrangel.controllers;

import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import own.luisrangel.dao.implement.ActorImplement;
import own.luisrangel.javabeans.Actor;

/**
 *
 * @author ljrangeld
 */
@WebServlet(name = "ActoresController", urlPatterns = {"/actores"})
public class ActoresController extends HttpServlet {
        @Override
    public void init() throws ServletException {
        a = new ActorImplement();
        super.init();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String method = req.getMethod();
        String paramMethod = req.getParameter("_METHOD");

        if (paramMethod != null && !paramMethod.isEmpty()) {
            method = paramMethod.toUpperCase();
        }

        switch (method) {
            case "POST":
                doPost(req, resp);
                break;
            case "PUT":
                doPut(req, resp);
                break;
            case "DELETE":
                doDelete(req, resp);
                break;
            default:
                doGet(req, resp);
                break;
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            a.delete(Integer.valueOf(req.getParameter("id")));
        } catch (SQLException ex) {
            System.out.println("Servlet excepcion: " + ex.getMessage() + "\n" + req.getParameter("seek_txt"));
        }
        resp.sendRedirect("Actor/actores.jsp");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            a.update(new Actor(Integer.valueOf(req.getParameter("id_txt")), req.getParameter("name_txt")));
        } catch (SQLException ex) {
            System.out.println("Servlet excepcion: " + ex.getMessage() + "\n");
        }
        resp.sendRedirect("Actor/actores.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            session = request.getSession();
            if (String.valueOf(request.getParameter("isFilter")).equals("null") || request.getParameter("isFilter") == null
                    || request.getParameter("isFilter").isEmpty() || request.getParameter("seek_txt").isEmpty() || request.getParameter("seek_txt") == null) {
                request.setAttribute("act_list", a.selectAll());
            } else {
                request.setAttribute("act_list", a.selectFilter(request.getParameter("seek_txt")));
            }

            if (request.getParameter("id_actor") == null || String.valueOf(request.getParameter("id_actor")).equals("null") || request.getParameter("id_actor").isEmpty()) {
            } else {
                request.setAttribute("edit_actor", a.select(Integer.valueOf(request.getParameter("id_actor"))));
            }
        } catch (SQLException ex) {
            System.out.println("Excepcion al procesar get servlet: " + ex.getMessage());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            session = request.getSession();
            if (request.getParameter("name_txt").isEmpty() || request.getParameter("name_txt") == null) {
            } else {
                a.insert(new Actor(0, request.getParameter("name_txt")));
            }
        } catch (SQLException ex) {
            System.out.println("Servlet excepcion: " + ex.getMessage() + "\n");
        }
        response.sendRedirect("Actor/actores.jsp");
    }

    private ActorImplement a;
    private HttpSession session;

}
