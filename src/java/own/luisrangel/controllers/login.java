package own.luisrangel.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ljrangeld
 */
@WebServlet(name = "login", urlPatterns = {"/login"}, initParams = {
    @WebInitParam(name = "user", value = "sdominguezi")
    , @WebInitParam(name = "pass", value = "12345")})
public class login extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        session = request.getSession();

        if (request.getParameter("close") == null) {
        } else if (request.getParameter("close").equals("1")) {
            session.invalidate();
            response.sendRedirect("index.jsp");
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        session = request.getSession();
        if (request.getParameter("user").equals(getInitParameter("user"))
                && request.getParameter("password").equals(getInitParameter("pass"))) {
            session.setAttribute("user", request.getParameter("user"));
            session.setAttribute("access", true);
            //getServletContext().getRequestDispatcher("/index.jsp?isFilter=null&id_film=null&_METHOD=GET").forward(request, response);
            response.sendRedirect("index.jsp");
        } else {
            getServletContext().getRequestDispatcher("/login/login.jsp").forward(request, response);
        }

    }
    private HttpSession session;
}
