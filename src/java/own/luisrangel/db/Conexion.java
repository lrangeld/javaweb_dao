package own.luisrangel.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ljrangeld
 */
public class Conexion {

    //Constructor
    public Conexion() {
    }

    //Metodos de clase
    public static Connection getDBConection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conexion = DriverManager.getConnection(CONECTION_STRING, "root", "12345");

        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Excepcion al intentar obtener la conexión con la DB, mensaje:\n" + e.getMessage());
        }
        return conexion;
    }

    //Atributos de clase
    public static final String DB_SERVER = "127.0.0.1";
    public static final String DB_NAME = "Peliculas";
    public static final String DB_PORT = "3306";
    private static final String CONECTION_STRING = "jdbc:mysql://" + DB_SERVER + ":" + DB_PORT + "/" + DB_NAME + "?useSSL=false&serverTimezone=UTC";
    private static Connection conexion;
}
