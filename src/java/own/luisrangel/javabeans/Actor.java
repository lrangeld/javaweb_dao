package own.luisrangel.javabeans;

import java.io.Serializable;

/**
 *
 * @author ljrangeld
 */
public class Actor implements Serializable {

    //Constructores de clase
    public Actor() {
    }

    public Actor(int idActor, String name) {
        this.idActor = idActor;
        this.name = name;
    }

    //Metodos de clase
    public int getIdActor() {
        return idActor;
    }

    public void setIdActor(int idActor) {
        this.idActor = idActor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Atributos de clase
    private int idActor = 0;
    private String name = null;
}
