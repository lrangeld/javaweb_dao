package own.luisrangel.javabeans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ljrangeld
 */
public class Pelicula implements Serializable {

    //Contrcutores de clase
    public Pelicula() {
    }

    public Pelicula(int id, String name, int year, String synopsis, ArrayList<Genero> genero) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.synopsis = synopsis;
        this.genero = genero;
    }

    public Pelicula(int id, String name, int year, String synopsis) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.synopsis = synopsis;
        this.genero = null;
    }

    //Metodos de clase
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public ArrayList<Genero> getGenero() {
        return genero;
    }

    public void setGenero(ArrayList<Genero> genero) {
        this.genero = genero;
    }

    //Atributos de clase
    private int id = 0;
    private String name = null;
    private int year = 0;
    private String synopsis = "";
    private ArrayList<Genero> genero = null;
}
