package own.luisrangel.javabeans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author ljrangeld
 */
public class Genero implements Serializable {

    //Constructores de clase
    public Genero() {
    }

    public Genero(int idGenero, String name, ArrayList<Pelicula> pelicula) {
        this.idGenero = idGenero;
        this.name = name;
        this.pelicula = pelicula;
    }

    public Genero(int idGenero, String name) {
        this.idGenero = idGenero;
        this.name = name;
        this.pelicula = null;
    }

    //Metodos de clase
    public int getIdGenero() {
        return idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Pelicula> getPelicula() {
        return pelicula;
    }

    public void setPelicula(ArrayList<Pelicula> pelicula) {
        this.pelicula = pelicula;
    }

    //Atributos de clase
    private int idGenero = 0;
    private String name = null;
    private ArrayList<Pelicula> pelicula = null;

}
