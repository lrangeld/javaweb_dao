<%@page import="java.util.ArrayList" %>
<%@page import="own.luisrangel.javabeans.Pelicula" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Cinema</title>
    </head>
    <body>
        <% request.getRequestDispatcher("/peliculas").include(request, response);%>


        <div class="btn-group" role="group" aria-label="Basic example">
            <c:choose>
                <c:when test="${sessionScope.user != null}">                    
                    <a class="btn btn-info">Usuaio: ${sessionScope.user}</a>
                </c:when>
            </c:choose>
            <c:choose>
                <c:when test="${sessionScope.access == true}">                
                    <a href="${pageContext.request.contextPath}/Pelicula/add.jsp" class="btn btn-secondary" data-toggle="modal" data-target="#addp_modal">Agregar pelicula</a>
                    <a href="${pageContext.request.contextPath}/Actor/actores.jsp" class="btn btn-secondary">Ver actores</a>
                </c:when>
            </c:choose>

            <c:choose>
                <c:when test="${sessionScope.access == null}">                
                    <a href="${pageContext.request.contextPath}/login/login.jsp" class="btn btn-primary" data-toggle="modal" data-target="#login_modal">Iniciar Sesion</a>
                </c:when>
                <c:when test="${sessionScope.access == true}">                
                    <a href="${pageContext.request.contextPath}/login?close=1" class="btn btn-danger">Cerrar sesion</a>
                </c:when>
            </c:choose>

        </div>
        <hr>
        <div class="container">
            <div class="row justify-content-center justify-content-md-center ">
                <div class="col align-self-center col-md-auto">
                    <h1>Listado de Peliculas</h1></div>
            </div>
        </div>

        <div class="container">
            <form action="#" method="GET">           
                <div class="container">
                    <div class="row justify-content-center justify-content-md-center ">
                        <div class="col align-self-center col-md-auto">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Buscar:</span>
                                </div>
                                <input type="text" name="seek_txt" id="serchtxt" class="form-control" placeholder="Genero o año de la pelicula" aria-label="Genero o año" aria-describedby="basic-addon1" value="${param['seek_txt']}">
                            </div>
                        </div>

                            <div class="col align-self-center col-md-auto">
                                <input type="hidden" value ="1" name="isFilter"/>
                                <input type="submit" name="seek_btn" value="buscar..." class="btn btn-secondary"/>
                            </div>
                    </div>
                </div>
            </form>
            <hr>
            <table class="table" >
                <thead class="thead-dark">
                    <tr >
                        <th scope="col">
                            Nombre
                        </th>
                        <th scope="col">
                            Año
                        </th>
                        <th scope="col">
                            Sinopsis
                        </th>
                        <c:choose>
                            <c:when test="${sessionScope.access == true}">                
                                <th scope="col" colspan="2">
                                    Accion
                                </th>
                            </c:when>
                        </c:choose>
                    </tr>            
                </thead>
                <tbody>
                    <c:forEach items="${requestScope.peliculas}" var="p">
                        <tr>
                            <td scope="row">
                                ${p.name}
                            </td>
                            <td scope="row">
                                ${p.year}
                            </td>
                            <td scope="row">
                                ${p.synopsis}
                            </td>
                            <c:choose>
                                <c:when test="${sessionScope.access == true}">                
                                    <td scope="row">
                                        <!--Pelicula/update.jsp-->
                                        <a href="Pelicula/update.jsp?id_film=${p.id}">Editar</a>
                                    </td>
                                    <td scope="row">
                                        <a href="peliculas?id=${p.id}&_METHOD=delete"  
                                           onclick="return confirm('¿Estas seguro de Elimnar el registro?')">Borrar</a>
                                    </td>
                                </c:when>
                            </c:choose>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>

        </div>
        <!--Modal Login-->
        <div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Acceso a Sistema</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action = "${pageContext.request.contextPath}/login" method="POST">
                            <div class="form-group">
                                <label for="user">Usuario: </label>
                                <input type="text"  id="usr" name="user" placeholder="Usuario" class="form-control">  
                                <label for="password">Contraseña:  </label>
                                <input type="password"  id="pass" name="password" placeholder="Contraseña" class="form-control"> 
                                <div class="form-group form-check">
                                    <input type="checkbox" class="form-check-input" id="rem" name="rem_usr" value ="recordar">
                                    <label class="form-check-label" for="exampleCheck1">Recordar usuario</label>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" value="Enviar" class="btn btn-primary"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Fin modal Login-->

        <!--Modal agregar pelicula-->
        <div class="modal fade" id="addp_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Agregar pelicula</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="${pageContext.request.contextPath}/peliculas" method="POST">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nombre:</span>
                                </div>
                                <input type="text" name="film_txt" class="form-control" placeholder="Nombre de pelicula" aria-label="Nombre de pelicula" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Sinopsis:</span>
                                </div>
                                <input type="text" name="syn_txt" class="form-control" placeholder="Descripción de la pelicula" aria-label="Descripción de la pelicula" aria-describedby="basic-addon1">
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Año:</span>
                                </div>
                                <input type="text" name="year_txt" class="form-control" placeholder="Año de estreno" aria-label="Año de estreno" aria-describedby="basic-addon1">
                            </div>

                    </div>
                    <div class="modal-footer">

                        <input type="submit" name="add_btn" value="Agregar" class="btn btn-primary"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Fin modal agregar pelicula-->

        <!--Modal actualizar pelicula-->
        <!--Fin modal actualizar pelicula-->

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
