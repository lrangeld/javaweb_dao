<%@page import="own.luisrangel.javabeans.Pelicula"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:include page="/peliculas"/>      

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Modificar Pelicula</title>
    </head>
    <body>
        <div class="container">
            <div class="container">
                <div class="row justify-content-center justify-content-md-center ">
                    <div class="col align-self-center col-md-auto">
                        <h1>Actualizar Datos de Pelicula</h1>
                    </div>
                </div>
            </div>
            <form action="${pageContext.request.contextPath}/peliculas" method="POST">            
                <input type="hidden" name="_METHOD" value="PUT"/>
                <input type="hidden" name="id" value="${requestScope.edit_film.id}"/>
                <div class="row justify-content-center justify-content-md-center ">
                    <div class="col align-self-center col-md-auto">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Nombre:</span>
                            </div>
                            <input type="text" name="film_txt" class="form-control" placeholder="Nombre de pelicula" aria-label="Nombre de pelicula" aria-describedby="basic-addon1" value="${requestScope.edit_film.name}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center justify-content-md-center ">
                    <div class="col align-self-center col-md-auto">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Sinopsis:</span>
                            </div>
                            <input type="text" name="syn_txt" class="form-control" placeholder="Descripción de la pelicula" aria-label="Descripción de la pelicula" aria-describedby="basic-addon1" value="${requestScope.edit_film.synopsis}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center justify-content-md-center ">
                    <div class="col align-self-center col-md-auto">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Año:</span>
                            </div>
                            <input type="text" name="year_txt" class="form-control" placeholder="Año de estreno" aria-label="Año de estreno" aria-describedby="basic-addon1" value="${requestScope.edit_film.year}">
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center justify-content-md-center ">
                    <div class="col align-self-center col-md-auto">

                        <input type="submit" name="add_btn" value="Guardar" class="btn btn-primary"/>
                    </div>
                    <div class="col align-self-center col-md-auto">
                        <input type="button" value="Cancelar" onclick="window.history.back();" class="btn btn-danger"/>
                    </div>
                </div>
            </form>
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
