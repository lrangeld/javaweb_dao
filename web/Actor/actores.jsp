<%@page import="java.util.ArrayList" %>
<%@page import="own.luisrangel.javabeans.Actor" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Cinema</title>
    </head>
    <body>
        <% request.getRequestDispatcher("/actores").include(request, response);%>
        <div class="btn-group" role="group" aria-label="Basic example">
            <c:choose>
                <c:when test="${sessionScope.user != null}">                    
                    <a class="btn btn-info">Usuaio: ${sessionScope.user}</a>
                </c:when>
            </c:choose>
            <c:choose>
                <c:when test="${sessionScope.access == true}">                
                    <a href="${pageContext.request.contextPath}/Actor/add.jsp" class="btn btn-secondary" data-toggle="modal" data-target="#adda_modal">Agregar actor</a>
                    <a href="${pageContext.request.contextPath}/index.jsp" class="btn btn-secondary">Ver peliculas</a>
                </c:when>
            </c:choose>

            <c:choose>
                <c:when test="${sessionScope.access == null}">                
                    <a href="${pageContext.request.contextPath}/login/login.jsp" class="btn btn-primary">Iniciar Sesion</a>
                </c:when>
                <c:when test="${sessionScope.access == true}">                
                    <a href="${pageContext.request.contextPath}/login?close=1" class="btn btn-danger">Cerrar sesion</a>
                </c:when>
            </c:choose>
        </div>
        <hr>

        <div class="container">
            <div class="row justify-content-center justify-content-md-center ">
                <div class="col align-self-center col-md-auto">
                    <h1>Listado de Actores</h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="container">
                <form action="#" method="GET">
                    <div class="row justify-content-center justify-content-md-center ">
                        <div class="col align-self-center col-md-auto">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Buscar:</span>
                                </div>
                                <input type="text" name="seek_txt" id="serchtxt" class="form-control" placeholder="Nombre del actor" aria-label="Nombre del actor" aria-describedby="basic-addon1" value="${param['seek_txt']}">
                            </div>
                        </div>                            
                        <div class="col align-self-center col-md-auto">
                            <input type="hidden" value ="1" name="isFilter"/>
                            <input type="submit" name="seek_btn" value="buscar..." class="btn btn-secondary"/>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">
                            Nombre
                        </th>

                        <c:choose>
                            <c:when test="${sessionScope.access == true}">                
                                <th scope="col" colspan="2">
                                    Accion
                                </th>
                            </c:when>
                        </c:choose>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${requestScope.act_list}" var="a">
                        <tr>
                            <td scope="row">
                                ${a.name}
                            </td>
                            <c:choose>
                                <c:when test="${sessionScope.access == true}">                
                                    <td scope="row">
                                        <a href="${pageContext.request.contextPath}/Actor/update.jsp?id_actor=${a.idActor}">Editar</a>
                                    </td>

                                    <td scope="row">
                                        <a href="${pageContext.request.contextPath}/actores?id=${a.idActor}&_METHOD=delete"  
                                           onclick="return confirm('¿Estas seguro de Elimnar el registro?')">Borrar</a>
                                    </td>
                                </c:when>
                            </c:choose>

                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>

        <!--Modal insetar actor-->
        <div class="modal fade" id="adda_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Agregar actor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="${pageContext.request.contextPath}/actores" method="POST">

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">Nombre:</span>
                                </div>
                                <input type="text" name="name_txt" id="serchtxt" class="form-control" placeholder="Nombre del actor" aria-label="Nombre del actor" aria-describedby="basic-addon1">
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="add_btn" value="Agregar" class="btn btn-primary"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--Fin modal insertar actor-->

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
